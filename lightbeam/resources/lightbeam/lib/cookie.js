const {
  Cc, Ci, Cr
} = require('chrome');
const Request = require('sdk/request').Request;
const {
  on, off, emit
} = require('sdk/event/core');
const {
  data
} = require("sdk/self");
const addonUrl = data.url("index.html");
const persist = require("./persist");
const {
  setTimeout
} = require("sdk/timers");
const ss = require('sdk/simple-storage');
// An array of connection objects serialized to an array.
var cookieBatch = [];
const cookieBatchSize = 2;

var eTLDSvc = Cc["@mozilla.org/network/effective-tld-service;1"].
getService(Ci.nsIEffectiveTLDService);

const {
  getTabForChannel
} = require('./tab/utils');

exports.Cookie = Cookie;
exports.addCookie = addCookie;

function addCookie(cookie) {
  cookieBatch.push(cookie.toLog());
  if (cookieBatch.length == cookieBatchSize) {
   flushToStorage();
  }
  console.debug("got", cookieBatch.length, "cookies");
}

exports.getAllCookies = function getAllCookies() {
  console.debug("got", cookieBatch.length, "buffered connections", ss.storage.cookies.length, "persisted connections");
  return ss.storage.cookies.concat(cookieBatch);
};



function Cookie() {
	//this.isNew = true;
}	
	// subject comes from events.on("http-on-modify-request");
Cookie.fromSubject = function (subject) {
  var cookie1 = new Cookie();
  cookie1.restoreFromSubject(subject);
  return cookie1;
};

Cookie.prototype.restoreFromSubject = function (event) {
  try {
          //tab = getTabForChannel(channel);
	  var cookieChannel = event.subject.QueryInterface(Ci.nsICookie);
	  if ((event.data == 'added') || (event.data == 'changed')) {		
		this.valid = true;  
		this.expiry = cookieChannel.expires;
		this.name = cookieChannel.name;
		this.value  = cookieChannel.value;
		this.host = cookieChannel.host;    
		this.isDomain = cookieChannel.isDomain;
		this.isSecure = cookieChannel.isSecure;
		  
	  
		var cookieChannel = event.subject.QueryInterface(Ci.nsICookie2); 
		//this.expiry = cookieChannel.expires;
		this.isHttpOnly = cookieChannel.isHttpOnly;
		this.isSession = cookieChannel.isSession;
		this.lastAccessed = cookieChannel.lastAccessed;
		this.rawHost = cookieChannel.rawHost;	
		//this.name = cookieChannel.name;
		//this.value  = cookieChannel.value;
		//this.host = cookieChannel.host;  
		/*this.isNew = persist.isNewCookie(cookieChannel.name);  
		if(this.isNew){
			console.debug("new cookie");

			}*/

	/*	cookieChannel = aSubject.QueryInterface(Components.interfaces.nsICookie);
		this.expires = cookieChannel.expires;
		this.host = cookieChannel.host;
		this.isDomain = cookieChannel.isDomain;
		this.name = cookieChannel.name;
		//console.error("**cookie-name "+cookieChannel.name);
		this.path = cookieChannel.path;
		//console.error("**cookie-path"+this.path);
		this.value  = cookieChannel.value;
		this.isSecure = cookieChannel.isSecure;
		this.policy = cookieChannel.policy;
		this.status = cookieChannel.status;*/
	 }
	 else if ((event.data == 'deleted')) {
		 this.valid = false;
		 
	}		
  } catch (e) {
	this.message = e.message();
	console.debug('EXCEPTION CAUGHT: ');    
  }	
	
};

Cookie.prototype.toLog = function () {
  var theLog = [
    this.name,
    this.value,
    this.host,
    this.rawHost,
    this.expiry,
    this.isSession,
    this.isHttpOnly,
    this.isDomain,
    this.isSecure,
    this.lastAccessed
  ];
  return theLog;
};

function flushToStorage() {
  console.debug("flushing", cookieBatch.length, "buffered cookies");
  persist.storeCookies(cookieBatch);
  cookieBatch.length = 0;
}
// Every 5 minutes, flush to storage.
setTimeout(flushToStorage, 5 * 60 * 1000);
